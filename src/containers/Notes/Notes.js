import React, {Component} from 'react'
import './Notes.css'
import NotesForm from "../../components/Notes/NotesForm/NotesForm";
import NotesList from "../../components/Notes/NotesList/NotesList";
import axios from "axios/index";
import Spinner from "../../components/UI/Spinner/Spinner";

class Notes extends Component {
	
	state = {
		notes: [],
		titleValue: '',
		textValue: '',
		loading: false
	};
	
	addNoteTitleHandler = (e) => {
		let titleValue = this.state.titleValue;
		titleValue = e.target.value;
		this.setState({titleValue});
	};
	
	addNoteTextHandler = (e) => {
		let textValue = this.state.textValue;
		textValue = e.target.value;
		this.setState({textValue});
	};
	
	addNoteHandler = () => {
		let noteTitle = this.state.titleValue;
		let noteText = this.state.textValue;
		let note = {title: noteTitle, text: noteText, disabled: true};
		axios.post('/notes.json', note).finally(() => {
			this.updateNotes();
			this.setState(prevState => {
				return {
					titleValue: prevState.titleValue = '',
					textValue: prevState.textValue = ''
				}
			});
		});
	};
	
	updateNotes = async () => {
		this.setState({loading: true});
		let notes = await axios.get('/notes.json').finally(() => {
			this.setState({loading: false});
		});
		let newNotes = [];
		for (let key in notes.data) {
			newNotes.push({
				title: notes.data[key].title,
				id: key,
				text: notes.data[key].text,
				disabled: notes.data[key].disabled
			});
		}
		this.setState({notes: newNotes});
	};
	
	editNoteTextHandler = (event, id) => {
		const notes = [...this.state.notes];
		const index = notes.findIndex(p => p.id === id);
		const note = {...this.state.notes[index]};
		note.text = event.target.value;
		note.disabled = false;
		notes[index] = note;
		this.setState({notes});
	};
	
	saveNoteHandler = (id) => {
		const notes = [...this.state.notes];
		const index = notes.findIndex(p => p.id === id);
		this.setState({loading: true});
		notes[index].disabled = true;
		axios.put(`/notes/${id}.json`, notes[index]).finally(() => this.setState({loading: false}));
	};
	
	deleteNoteHandler = (id) => {
		const notes = [...this.state.notes];
		const index = notes.findIndex(p => p.id === id);
		this.setState({loading: true});
		axios.delete(`/notes/${id}.json`).finally(() => {
			notes.splice(index, 1);
			this.setState({notes});
			this.setState({loading: false});
		});
	};
	
	componentDidMount() {
		this.updateNotes();
	}
	
	render() {
		let notes = (
			<NotesList
				notes={this.state.notes}
				save={this.saveNoteHandler}
				delete={this.deleteNoteHandler}
				editNote={this.editNoteTextHandler}
			/>
		);
		if (this.state.loading) notes = <Spinner/>;
		return (
			<div className="Notes">
				<NotesForm
					title={this.state.titleValue}
					text={this.state.textValue}
					changeTitle={this.addNoteTitleHandler}
					changeText={this.addNoteTextHandler}
					addNote={this.addNoteHandler}
				/>
				{notes}
			</div>
		)
	}
}

export default Notes;