import React, {Component} from 'react'
import AddTaskForm from "../../components/ToDoList/AddTaskForm/AddTaskForm"
import Task from "../../components/ToDoList/Task/Task"
import axios from 'axios'
import Spinner from "../../components/UI/Spinner/Spinner";

class ToDoList extends Component {
	
	state = {
		task: [],
		inputValue: 'Add new task',
		loading: false
	};
	
	addTask = () => {
		const taskText = this.state.inputValue;
		let task = {text: taskText};
		const inputValue = 'Add new task';
		axios.post('/tasks.json', task).finally(() => this.updateTasks());
		this.setState({inputValue});
	};
	
	change = (event) => {
		let inputValue = [...this.state.inputValue];
		inputValue = event.target.value;
		this.setState({inputValue});
	};
	
	deleteTask = (id) => {
		const tasks = this.state.task;
		const index = tasks.findIndex(p => p.id === id);
		this.setState({loading: true});
		axios.delete(`/tasks/${id}.json`).finally(() => {
			tasks.splice(index, 1);
			this.setState({tasks, loading: false});
		});
	};
	
	resetFild = () => {
		let inputValue = this.state.inputValue;
		inputValue = '';
		this.setState({inputValue});
	};
	
	updateTasks = async () => {
		this.setState({loading: true});
		let tasks = await axios.get('/tasks.json').finally(() => {
			this.setState({loading: false});
		});
		let newTasks = [];
		for (let key in tasks.data) {
			newTasks.push({text: tasks.data[key].text, id: key});
		}
		this.setState({task: newTasks});
	};
	
	componentDidMount() {
		this.updateTasks();
	}
	
	render() {
		let tasks = (
			<div className="items">
				{this.state.task.map((task) => {
					return <Task
						key={task.id}
						text={task.text}
						status={task.status}
						delete={() => this.deleteTask(task.id)}
					/>
				})}
			</div>
		);
		if (this.state.loading) tasks = <Spinner/>;
		return (
			<div className="main">
				<div className="container">
					<AddTaskForm
						value={this.state.inputValue}
						add={() => this.addTask()}
						change={(event) => this.change(event)}
						click={this.resetFild}
					/>
					{tasks}
				</div>
			</div>
		);
	}
}

export default ToDoList;
