import React, {Component} from 'react'
import './MyFilmsList.css';
import Header from "../../components/MyFilmsList/Header/Header";
import Films from "../../components/MyFilmsList/Films/Films";
import Spinner from "../../components/UI/Spinner/Spinner";
import axios from "axios/index";

class MyFilmsList extends Component {
	
	state = {
		films: [],
		filmName: 'Film name',
		loading: false
	};
	
	changeName = (event) => {
		let filmName = this.state.filmName;
		filmName = event.target.value;
		this.setState({filmName});
	};
	
	addFilm = () => {
		let filmName = this.state.filmName;
		let newFilm = {name: filmName, disabled: true};
		axios.post('/films.json', newFilm).finally(() => this.updateFilms());
	};
	
	deleteFilm = (id) => {
		const films = [...this.state.films];
		const index = films.findIndex(p => p.id === id);
		this.setState({loading: true});
		axios.delete(`/films/${id}.json`).finally(() => {
			films.splice(index, 1);
			this.setState({films});
			this.setState({loading: false});
		});
	};
	
	resetName = () => {
		let filmName = this.state.filmName;
		filmName = '';
		this.setState({filmName});
	};
	
	editFilmName = (event, id) => {
		const films = [...this.state.films];
		const index = films.findIndex(p => p.id === id);
		const film = {...this.state.films[index]};
		film.name = event.target.value;
		film.disabled = false;
		films[index] = film;
		this.setState({films});
	};
	
	saveFilm = (id) => {
		const films = [...this.state.films];
		const index = films.findIndex(p => p.id === id);
		this.setState({loading: true});
		films[index].disabled = true;
		axios.put(`/films/${id}.json`, films[index]).finally(() => this.setState({loading: false}));
	};
	
	updateFilms = async () => {
		this.setState({loading: true});
		let films = await axios.get('/films.json').finally(() => {
			this.setState({loading: false});
		});
		let newFilms = [];
		for (let key in films.data) {
			newFilms.push({name: films.data[key].name, id: key, disabled: films.data[key].disabled});
		}
		this.setState({films: newFilms});
	};
	
	componentDidMount() {
		this.updateFilms();
	}
	
	render() {
		let films = (
			<Films
				editName={this.editFilmName}
				films={this.state.films}
				saveFilm={this.saveFilm}
				deleteFilm={this.deleteFilm}/>
		);
		if (this.state.loading) films = <Spinner/>;
		return (
			<div className="MyFilmsList">
				<Header
					changeName={(event) => this.changeName(event)}
					addFilm={() => this.addFilm()}
					filmName={this.state.filmName}
					resetName={(event) => this.resetName(event)}
				/>
				<div className="content">
					{films}
				</div>
			</div>
		)
	}
}

export default MyFilmsList;