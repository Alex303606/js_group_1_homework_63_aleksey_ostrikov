import React, {Component,Fragment} from 'react';
import {NavLink, Route, Switch} from "react-router-dom";
import ToDoList from "./containers/ToDoList/ToDoList";
import MyFilmsList from "./containers/MyFilmsList/MyFilmsList";
import './App.css'
import Notes from "./containers/Notes/Notes";
class App extends Component {
	render() {
		return (
			<Fragment>
				<header className="App-header">
					<NavLink exact to="/">ToDoList</NavLink>
					<NavLink to="/films">MyFilmsList</NavLink>
					<NavLink to="/notes">Notes</NavLink>
				</header>
				<Switch>
					<Route path="/" exact component={ToDoList}/>
					<Route path="/films" exact component={MyFilmsList}/>
					<Route path="/notes" exact component={Notes}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
