import React from 'react'

const NotesForm = props => {
	return (
		<div className="NotesFormHeader">
			<label htmlFor="title">Note title:</label>
			<input onChange={(e) => props.changeTitle(e)} value={props.title} id="title" type="text"/>
			<label htmlFor="text">Note text:</label>
			<textarea onChange={(e) => props.changeText(e)} value={props.text} id="text"/>
			<button onClick={props.addNote} className="addNote">Add Note</button>
		</div>
	)
};

export default NotesForm;