import React from 'react'
import Note from "./Note/Note";

const NotesList = props => {
	return (
		<ul className="NotesList">
			{
				props.notes.map(note => {
					return (
						<Note
							key={note.id}
							title={note.title}
							text={note.text}
							disabled={note.disabled}
							save={() => props.save(note.id)}
							delete={() => props.delete(note.id)}
							edit={(event) => props.editNote(event, note.id)}
						/>
					)
				})
			}
		</ul>
	)
};

export default NotesList;