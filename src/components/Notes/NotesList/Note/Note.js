import React from 'react'

const Note = props => {
	return (
		<li>
			<div className="noteTitle">Title: {props.title}</div>
			<textarea onChange={props.edit} value={props.text} className="noteText"/>
			<div className="note__button__block">
				<button
					disabled={props.disabled}
					onClick={props.save}
					className="saveNote">
					<span><i className="fas fa-save"/></span>
					Save edits
				</button>
				<button
					onClick={props.delete}
					className="deleteNote">
					<span><i className="fas fa-trash-alt"/></span>
					Delete note
				</button>
			</div>
		</li>
	)
};

export default Note;