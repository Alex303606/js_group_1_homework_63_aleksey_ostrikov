import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => {
	return (
		<header className="AddTaskFormHeader">
			<input onChange={props.change} onClick={props.click} value={props.value} type="text"/>
			<button onClick={props.add}>ADD</button>
		</header>
	)
};

export default AddTaskForm;