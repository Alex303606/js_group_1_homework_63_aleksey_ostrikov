import React from 'react';
import Film from "./Film/Film";

const Films = props => {
	return (
		<ul>
			{
				props.films.map(film => {
					return <Film
						key={film.id}
						name={film.name}
						deleteFilm={props.deleteFilm}
						saveFilm={() => props.saveFilm(film.id)}
						id={film.id}
						editName={(event) => props.editName(event, film.id)}
						disabled={film.disabled}
					/>;
				})
			}
		</ul>
	)
};

export default Films;