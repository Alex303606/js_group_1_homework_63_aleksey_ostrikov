import React, {Component} from 'react';

class Header extends Component {
	
	shouldComponentUpdate(newProps, newState) {
		return (newProps.filmName !== this.props.filmName);
	}
	
	render() {
		return (
			<header>
				<input
					onClick={this.props.resetName}
					onChange={this.props.changeName}
					className="name" type="text"
					value={this.props.filmName}
				/>
				<button onClick={this.props.addFilm} className="add">Add</button>
			</header>
		)
	}
}

export default Header;